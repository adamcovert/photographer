var ready = function ( fn ) {
  if ( typeof fn !== 'function' ) return;
  if ( document.readyState === 'interactive' || document.readyState === 'complete' ) {
    return fn();
  }
  document.addEventListener( 'DOMContentLoaded', fn, false );
};

var isInViewport = function (elem) {
  var distance = elem.getBoundingClientRect();
  return (
    distance.top >= 0 &&
    distance.left >= 0 &&
    distance.bottom <= (window.innerHeight || document.documentElement.clientHeight) &&
    distance.right <= (window.innerWidth || document.documentElement.clientWidth)
  );
};

var getSiblings = function (elem) {
  var siblings = [];
  var sibling = elem.parentNode.firstChild;
  for (; sibling; sibling = sibling.nextSibling) {
    if (sibling.nodeType === 1 && sibling !== elem) {
      siblings.push(sibling);
    }
  }
  return siblings;
};

var getOffsetTop = function (elem) {
  var location = 0;
  if (elem.offsetParent) {
    do {
      location += elem.offsetTop; elem = elem.offsetParent;
    } while (elem);
  }
  return location >= 0 ? location : 0;
};