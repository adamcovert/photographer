// var start = function open () {
  window.addEventListener( 'load', showContent )
// };

// setTimeout(start, 2000);

var loadTimer = setTimeout(function (){
  showContent();
  window.removeEventListener('load', showContent);
}, 3000);

function showContent() {
  clearTimeout(loadTimer);
  var preload = document.querySelector('.preload');
  if ( preload.classList.contains('preload--is-active') ) {
    preload.classList.remove('preload--is-active');
  }
}