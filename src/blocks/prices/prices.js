ready(function () {

  var sectionPrices = document.querySelector('.prices');
  var distanceFromTop = getOffsetTop(sectionPrices);
  var viewportHeight = window.innerHeight;
  var halfOfViewportHeight = viewportHeight / 2;
  var body = document.querySelector('body');

  document.addEventListener('scroll', function (event) {

    var distance = window.pageYOffset;

    if ( body.classList.contains('bg-change') ) {
      body.classList.remove('bg-change');
    }

    if ( distance > ( distanceFromTop - halfOfViewportHeight ) ) {
      body.classList.add('bg-change');
    } else if ( distance < ( distanceFromTop - halfOfViewportHeight ) ) {
      body.classList.remove('bg-change');
    }

  });

});