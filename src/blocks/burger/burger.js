ready(function () {

  // Open and Close Main Navigation block
  var burger = document.querySelector('.burger');
  var navigationMenu = document.querySelector('.nav-menu');
  var underLayer = document.querySelector('.underlayer');
  var body = document.querySelector('body');

  burger.addEventListener('click', function (event) {

    event.preventDefault();

    // Close
    if (navigationMenu.classList.contains('nav-menu--is-active')) {
      burger.classList.remove('burger--is-active');
      navigationMenu.classList.remove('nav-menu--is-active');
      underLayer.classList.remove('underlayer--is-active');
      body.classList.remove('prevent-scroll');
      return;
    }

    // Open
    navigationMenu.classList.add('nav-menu--is-active');
    burger.classList.add('burger--is-active');
    underLayer.classList.add('underlayer--is-active');
    body.classList.add('prevent-scroll');
  });

});