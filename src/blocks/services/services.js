ready(function () {

  var sectionOne = document.querySelector('.services__cover.services__cover--portrait')
  var elementOne = document.querySelector('.services__section.services__section--portrait')

  var sectionTwo = document.querySelector('.services__cover.services__cover--fashion')
  var elementTwo = document.querySelector('.services__section.services__section--fashion')

  var sectionThree = document.querySelector('.services__cover.services__cover--wedding')
  var elementThree = document.querySelector('.services__section.services__section--wedding')

  var sectionFour = document.querySelector('.services__cover.services__cover--around')
  var elementFour = document.querySelector('.services__section.services__section--around')

  var changePhoto = function (elem, sibling) {
    if ( isInViewport(elem) ) {
      var siblings = getSiblings(elem);
      siblings.forEach(function (covers) {
        if ( covers.classList.contains('services__section--is-active') ) {
          covers.classList.remove('services__section--is-active')
        }
      });
      sibling.classList.add('services--is-active')
      elem.classList.add('services__section--is-active')
    } else {
      sibling.classList.remove('services--is-active')
      elem.classList.remove('services__section--is-active')
    };
  };

  window.addEventListener('scroll', function () {
    changePhoto(elementOne, sectionOne);
    changePhoto(elementTwo, sectionTwo);
    changePhoto(elementThree, sectionThree);
    changePhoto(elementFour, sectionFour);
  });

});